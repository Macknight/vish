package com.macknighter.vish;


import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

/*
    * Created by rodrigo.poloni on 12/07/16.
            */
    public class PlayWidgetProvider extends AppWidgetProvider {
        private static final String TAG = "PlayWidgetProvider";
        @Override
        public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds ){
            int widgetId = appWidgetIds[0];// theres only one widget bound

            Intent intentPlaySound = new Intent();
            intentPlaySound.setAction("com.macknighter.vish.PLAY_SOUND");
            PendingIntent playPendingIntent = PendingIntent.getBroadcast(context, 0, intentPlaySound, 0);


            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.lock_widget);
            views.setOnClickPendingIntent(R.id.actionButton, playPendingIntent);

            appWidgetManager.updateAppWidget(widgetId, views);

        }



    }
