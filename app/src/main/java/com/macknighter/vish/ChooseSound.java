package com.macknighter.vish;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

/**
 * Created by rodrigo.poloni on 13/07/16.
 */
public class ChooseSound  extends Activity{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose);
    }


   public void onCheckboxClicked(View view){
       if(view.getId() == findViewById(R.id.vish).getId()){
           MainActivity.setSoundId(R.raw.mtatreta);
       }else if(view.getId() == findViewById(R.id.everyday).getId()){
           MainActivity.setSoundId(R.raw.weedeveryday);
       }else if(view.getId() == findViewById(R.id.aisafada).getId()){
           MainActivity.setSoundId(R.raw.aisafada);
       }else if(view.getId() == findViewById(R.id.tranquilo).getId()){
            MainActivity.setSoundId(R.raw.tranquilo);
       }


       finish();

   }
}
