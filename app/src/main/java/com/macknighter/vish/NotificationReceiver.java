package com.macknighter.vish;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.util.Log;

/**
 * Created by leandro on 7/10/16.
 */
public class NotificationReceiver extends BroadcastReceiver {

    private static final String TAG = "NotificationReceiver";

    private static int soundId =  R.raw.mtatreta;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "receive broadcast");
        MediaPlayer mp = MediaPlayer.create(context, soundId);
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mp.start();
    }

    public static int getSoundId() {
        return soundId;
    }

    public static void setSoundId(int soundIde) {
        soundId = soundIde;
    }
}