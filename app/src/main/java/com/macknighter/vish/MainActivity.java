package com.macknighter.vish;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.widget.ImageButton;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

public class MainActivity extends AppCompatActivity {

    private static int soundId =   R.raw.mtatreta;

    private AdView mAdView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest =new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                .addTestDevice("30AA2309211FBFA9724693A6586D9057")
                .build();
        mAdView.loadAd(adRequest);




    }

    @Override
    protected void onStart() {

        ImageButton one = (ImageButton) this.findViewById(R.id.button);
        final MediaPlayer mp = MediaPlayer.create(this, soundId);
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        one.setOnClickListener(new View.OnClickListener(){

            public void onClick(View v) {
                mp.start();
            }
        });

        super.onStart();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {//SE FOR CELULAR DE PLAYBA

            NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

            Intent intentPlaySound = new Intent();

            //the PLAY_SOUND action is bind to the
            //Notification Receiver class (onReceive method)
            intentPlaySound.setAction("com.macknighter.vish.PLAY_SOUND");
            PendingIntent playPendingIntent = PendingIntent.getBroadcast(this, 0, intentPlaySound, 0);


            Notification.Builder builder = new Notification.Builder(this);


            builder.setVisibility(Notification.VISIBILITY_PUBLIC);


            builder.addAction(R.drawable.play_icon, "PLAY", playPendingIntent);
            builder.setSmallIcon(R.drawable.icon);
            builder.setContentTitle("VISH Muita Treta");
            builder.setLargeIcon(BitmapFactory.decodeResource(getResources(),R.drawable.icon));
            Notification notification = builder.build();
            nm.notify(R.drawable.icon, notification);

        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){//SE FOR CELULAR DO GUETO
            Log.e("android version:", "jelly bean range");
            }


            }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }



    //static getters and setters for the soundID
    //allows to easily change soundId from the options menu
    public static int getSoundId() {
        return soundId;
    }




    public static void setSoundId(int soundIde) {
       soundId = soundIde;
       NotificationReceiver.setSoundId(soundIde);
    }


    public void callChooseSoundScreen(View view){
        Intent intent = new Intent(this, ChooseSound.class);
        startActivity(intent);

    }
}
